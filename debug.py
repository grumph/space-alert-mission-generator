# -*- coding: utf-8 -*-
#!/usr/bin/env python

from __future__ import print_function
import inspect
import sys

################################################################################
# Debug library
################################################################################
DEBUG=False

def debug(message, *args):
    if DEBUG:
        print(message % args, file=sys.stderr)


def TODO(message=''):
    end = ''
    if message:
        end = ' : ' + message
    debug('TODO ' + inspect.stack()[1][3] + end)

