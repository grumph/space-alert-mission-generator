# -*- coding: utf-8 -*-
#!/usr/bin/env python

import sys
from PyQt4 import QtGui, QtCore
import config
import trad


def add_components(conf, vbox):
    for c in conf:
        if isinstance(conf[c], config.Bool):
            w = QtGui.QCheckBox(trad.get_sentence(c))
            if conf[c].value:
                w.toggle()
            w.setToolTip(trad.get_description(c))
            vbox.addWidget(w)
        elif isinstance(conf[c], (config.MinMax, config.MinMaxRange)):
            vbox.addWidget(QtGui.QLabel(trad.get_sentence(c)))
            l = QtGui.QHBoxLayout()
            qle = QtGui.QLineEdit()
            qle.setText(str(conf[c].min))
            l.addWidget(qle)
            if isinstance(conf[c], config.MinMaxRange):
                l.addWidget(QtGui.QSlider(QtCore.Qt.Horizontal))
            qle = QtGui.QLineEdit()
            qle.setText(str(conf[c].max))
            l.addWidget(qle)
#            l.setToolTip(trad.get_description(c))
            vbox.addLayout(l)


app = QtGui.QApplication(sys.argv)
window = QtGui.QWidget()

vbox = QtGui.QVBoxLayout()
conf = config.default()
add_components(conf, vbox)
window.setLayout(vbox)

qr = window.frameGeometry()
qr.moveCenter(QtGui.QDesktopWidget().availableGeometry().center())
window.move(qr.topLeft())

window.setWindowTitle("Space Alert mission generator")
window.show()

sys.exit(app.exec_())


def set_last_config():
    pass

def set_default():
    pass
