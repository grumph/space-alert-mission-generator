# -*- coding: utf-8 -*-
#!/usr/bin/env python

from __future__ import print_function
from pathlib import Path
from pydub import AudioSegment
from xml.etree.ElementTree import parse


BASEDIR = 'clips'

def available_languages():
    res = [x.name for x in Path(BASEDIR).iterdir() if x.is_dir()]
    return res

def load(directory = str(available_languages()[0])):
    print('Clips : Import', directory)
    c = [('begin_first_phase', 'AnnounceBeginFirstPhase'),                              # First phase
         ('first_phase_ends_in_1_minute', 'AnnounceFirstPhaseEndsInOneMinute'),
         ('first_phase_ends_in_20_seconds', 'AnnounceFirstPhaseEndsInTwentySeconds'),
         ('first_phase_ends', 'AnnounceFirstPhaseEnds'),
         ('second_phase_begins', 'AnnounceSecondPhaseBegins'),                          # Second phase
         ('second_phase_ends_in_1_minute', 'AnnounceSecondPhaseEndsInOneMinute'),
         ('second_phase_ends_in_20_seconds', 'AnnounceSecondPhaseEndsInTwentySeconds'),
         ('second_phase_ends', 'AnnounceSecondPhaseEnds'),
         ('third_phase_begins', 'AnnounceThirdPhaseBegins'),                            # Third phase
         ('operation_ends_in_1_minute', 'AnnounceThirdPhaseEndsInOneMinute'),
         ('operation_ends_in_20_seconds', 'AnnounceThirdPhaseEndsInTwentySeconds'),
         ('operation_ends', 'AnnounceThirdPhaseEnds'),
         ('communications_down', 'CommunicationsDownHeader'),                           # Communications problems
         ('white_noise', 'CommunicationsDownNoise'),
         ('communications_restored', 'CommunicationsDownFooter'),
         ('data_transfer', 'DataTransfer'),                                             # Data
         ('incoming_data', 'IncomingData'),
         ('time_t_plus_1', 'TimeTPlus1'),                                               # Time
         ('time_t_plus_2', 'TimeTPlus2'),
         ('time_t_plus_3', 'TimeTPlus3'),
         ('time_t_plus_4', 'TimeTPlus4'),
         ('time_t_plus_5', 'TimeTPlus5'),
         ('time_t_plus_6', 'TimeTPlus6'),
         ('time_t_plus_7', 'TimeTPlus7'),
         ('time_t_plus_8', 'TimeTPlus8'),
         ('red_alert_0', 'RedAlertLevel0'),                                             # Background
         ('red_alert_1', 'RedAlertLevel1'),
         ('red_alert_2', 'RedAlertLevel2'),
         ('red_alert_3', 'RedAlertLevel3'),
         ('alert', 'AlertHeader'),                                                      # Threats
         ('repeat', 'Repeat'),
         ('unconfirmed_report', 'UnconfirmedReport'),
         ('internal_threat', 'InternalThreat'),
         ('serious_internal_threat', 'SeriousInternalThreat'),
         ('threat', 'Threat'),
         ('serious_threat', 'SeriousThreat'),
         ('zone_blue', 'ZoneBlue'),
         ('zone_red', 'ZoneRed'),
         ('zone_white', 'ZoneWhite')]
    clips = {}
    tree = parse(str(Path(BASEDIR + '/' + directory + '/grammar.xml')))
    xmlroot = tree.getroot()
    for title, xml in c:
        print(' -', title)
        clips[title] = {'mp3': AudioSegment.from_mp3(str(Path(BASEDIR + '/' + directory + '/' + title + '.mp3'))),
                        'text': xmlroot.find(xml).text}
    return clips


if __name__ == "__main__":
    load()
