# -*- coding: utf-8 -*-
#!/usr/bin/env python

from __future__ import print_function
from xml.etree.ElementTree import parse
import debug

class MinMax:
    def __init__(self, min_value = -1, max_value = -1):
        self.min = min_value
        self.max = max_value
    def __eq__(self, minmax):
        return isinstance(minmax, MinMax) and self.min == minmax.min and self.max == minmax.max
    def __str__(self):
        return '(' + str(self.min) + ',' + str(self.max) + ')'

class MinMaxRange:
    def __init__(self, min_value = -1, max_value = -1, threshold_value = -1):
        self.min = min_value
        self.max = max_value
        self.threshold = threshold_value
    def __eq__(self, minmaxrange):
        return isinstance(minmaxrange, MinMaxRange) and self.min == minmaxrange.min and self.max == minmaxrange.max and self.threshold == minmaxrange.threshold
    def __str__(self):
        return '(' + str(self.min) + ',' + str(self.max) + ')[' + str(self.threshold) + ']'

class Bool:
    def __init__(self, bool_value):
        self.value = bool_value
    def __eq__(self, b):
        return (isinstance(b, Bool) and self.value == b.value) or (isinstance(b, bool) and self.value == b)
    def __str__(self):
        return str(self.value)

_default_config = [('MissionThreatLevel', 8, 8),
                   ('UnconfirmedThreatLevel', 1, 1),
                   ('UnconfirmedInternalThreat', 0, 1, 1),
                   ('SeriousThreat', 0, 2, 2),
                   ('InternalThreatLevel', 2, 3),
                   ('IncomingData', 2, 3),
                   ('DataTransfer', 3, 4),
                   ('TotalCommunicationFailureDuration', 45, 60),
                   ('SingleCommunicationFailureDuration', 9, 20),
                   ('PhaseOneDuration', 205, 240),
                   ('PhaseTwoDuration', 180, 225),
                   ('PhaseThreeDuration', 140, 155),
                   ('TimeToFirstConfirmedThreat', 0, 20000),
                   ('SanerInternalThreats', True),
                   ('SanerSeriousExternalThreats', True),
                   ('DoubleActionMission', False)]

def default():
    config = {}
    for data in _default_config:
        if len(data) == 2:
            value = Bool(data[1])
        elif len(data) == 3:
            value = MinMax(data[1], data[2])
        elif len(data) == 4:
            value = MinMaxRange(data[1], data[2], data[3])
        config[data[0]] = value
    return config

default_config = default()

def from_xml(xml_file='DefaultMission.xml'):
    tree = parse(xml_file)
    tree.getroot()
    config = {}
    for c in tree.findall('Setting'):
        if c.text in ['True', 'False']:
            config[c.attrib['name']] = Bool(c.text == 'True')
        else:
            if c.attrib['name'][:8] in ['MinRange', 'MaxRange']:
                name = c.attrib['name'][8:]
                value = MinMaxRange()
                attribute = c.attrib['name'][:3].lower()
            elif c.attrib['name'][:9] == 'Threshold':
                name = c.attrib['name'][9:]
                value = MinMaxRange()
                attribute = c.attrib['name'][:9].lower()
            elif c.attrib['name'][:3] in ['Min', 'Max']:
                name = c.attrib['name'][3:]
                value = MinMax()
                attribute = c.attrib['name'][:3].lower()
            else:
                raise Error
            if not name in config:
                config[name] = value
            setattr(config[name], attribute, int(c.text))
    debug.TODO('check for missing fields')
    debug.TODO('Check for missing values')
    return config

def save_to_xml(xml_file):
    debug.TODO()

if __name__ == "__main__":
    defc = default()
    xmlc = from_xml()
    for k in set(xmlc.keys()) | set(defc.keys()):
        try:
            kdefc = defc[k]
        except KeyError:
            kdefc = 'UNDEFINED'
        try:
            kxmlc = xmlc[k]
        except KeyError:
            kxmlc = 'UNDEFINED'
        if kdefc == kxmlc:
            print(k, '=', kdefc)
        else:
            print('*', k, '=', '{default : ', kdefc, ', xml : ', kxmlc, '}')
