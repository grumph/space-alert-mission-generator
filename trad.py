# -*- coding: utf-8 -*-
#!/usr/bin/env python

from __future__ import print_function
import config
import debug

config_sentences = [('MissionThreatLevel', 'Niveau de menace', 'ce paramètre définit le niveau de menace du jeu.'),
                    ('UnconfirmedThreatLevel', 'Menaces externes non confirmées', ''),
                    ('UnconfirmedInternalThreat', 'Menaces internes non confirmées', ''),
                    ('SeriousThreat', 'Menaces sérieuses', ''),
                    ('InternalThreatLevel', 'Niveau de menaces internes', ''),
                    ('IncomingData', 'Nombre de données entrantes', ''),
                    ('DataTransfer', 'Nombre de transferts de données', ''),
                    ('TotalCommunicationFailureDuration', 'Durée totale des problèmes de communication', ''),
                    ('SingleCommunicationFailureDuration', 'Durée d\'un problème de communication', ''),
                    ('PhaseOneDuration', 'Durée de la phase 1', ''),
                    ('PhaseTwoDuration', 'Durée de la phase 2', ''),
                    ('PhaseThreeDuration', 'Durée de la phase 3', ''),
                    ('TimeToFirstConfirmedThreat', 'Moment d\'arrivée de la première menace', ''),
                    ('SanerInternalThreats', 'Menaces internes cohérentes', ''),
                    ('SanerSeriousExternalThreats', 'Menaces externes cohérentes', ''),
                    ('DoubleActionMission', 'Actions doubles', '')]

def get_sentence(config_str):
    for x in config_sentences:
        if x[0] == config_str:
            return x[1]

def get_description(config_str):
    for x in config_sentences:
        if x[0] == config_str:
            return x[2] + '. Valeur par défaut : ' + str(config.default_config[config_str])
